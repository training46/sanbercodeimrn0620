console.log('+++++ Soal 1 +++++');
function range(a, b) {
  var result = [];

  if (!a || !b) return -1;

  if (a > b) {
    while (a >= b) {
      result.push(a--);
    }
  } else if (a < b) {
    while (a <= b) {
      result.push(a++);
    }
  }

  return result;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1


console.log('+++++ Soal 2 +++++');
function rangeWithStep(a, b, c) {
  var result = [];

  if (!a || !b || !c) return -1;

  if (a > b) {
    while (a >= b) {
      result.push(a);
      a -= c;
    }
  } else {
    while (a <= b) {
      result.push(a);
      a += c;
    }
  }

  return result;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


console.log('+++++ Soal 3 +++++');
function sum(a, b, c) {
  if (!c) c = 1;
  if (!b) b = 1;
  var range = rangeWithStep(a, b, c);
  var result = 0;
  for (var i=0; i < range.length; i++) {
    result += range[i];
  }

  return result;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


console.log('+++++ Soal 4 +++++');
function dataHandling(multidim) {
  for (var i=0; i<multidim.length; i++) {
    var item = multidim[i];
    console.log('Nomor ID: ' + item[0]);
    console.log('Nama Lengkap: ' + item[1]);
    console.log('TTL: ' + item[2]);
    console.log('Hobi: ' + item[3]);
  }
  console.log('');
}
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ];
dataHandling(input);


console.log('+++++ Soal 5 +++++');
function balikKata(kata) {
  var result = ''
  for (var i = kata.length; i>0; i--) {
    result += kata[i-1];
  }
  return result;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I


console.log('+++++ Soal 6 +++++');
function dataHandling2(input) {
  input.splice(1, 1, input[1] + 'Elsharawy');
  input.splice(2, 1, 'Provinsi ' + input[2]);
  input.splice(4, 1, 'Pria', 'SMA Internasional Metro');
  console.log(input);

  var dateArray = input[3].split('/');
  var monthStr;
  switch (dateArray[1]) {
    case '01': monthStr = 'Januari'; break;
    case '02': monthStr = 'Februari'; break;
    case '03': monthStr = 'Maret'; break;
    case '04': monthStr = 'April'; break;
    case '05': monthStr = 'Mei'; break;
    case '06': monthStr = 'Juni'; break;
    case '07': monthStr = 'Juli'; break;
    case '08': monthStr = 'Agustus'; break;
    case '09': monthStr = 'September'; break;
    case '10': monthStr = 'Oktober'; break;
    case '11': monthStr = 'November'; break;
    case '12': monthStr = 'Desember'; break;
  }
  console.log(monthStr);

  dateArray.sort(function(a, b){return b-a});
  console.log(dateArray);

  var joined = input[3].split('/').join('-');
  console.log(joined);

  var name = '' + input[1];
  console.log(name.slice(0,15)); 
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
